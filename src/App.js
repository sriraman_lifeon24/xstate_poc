import React, { useState, useEffect, useMemo } from 'react';
import './App.css';
import { Machine, interpret, assign } from 'xstate';

export function useMachine(machine) {
  const [current, setCurrent] = useState(machine.initialState);

  const service = useMemo(
    () =>
      interpret(machine)
        .onTransition(state => {
          if(state.changed) {
            setCurrent(state);
          }
        })
        .start(),
      []
  );

  useEffect(() => {
    return () => service.stop();
  }, []);

  return [current, service.send];
}

function updateProfile () {
  return new Promise ((resolve, reject) => {
    resolve('Profile Updated Successfully');
  })
}


const profileStateMachine = Machine({
  id: 'PROFILE',
  initial: 'idle',
  context: {
    user: null,
    msg: ""
  },
  states: {
    idle: {
      on: {
        SUBMIT: [
          {
            target: "updating",
            cond: (ctx, event) => ((event.data.name !== '') && (event.data.email !== ''))
          },
          {
            target: "error",
            actions: assign({ msg: "Validation Failed" })
          }
        ]
      }
    },
    updating: {
      invoke: {
        id: 'updateProfile',
        src: () => updateProfile(),
        onDone: {
          target: 'idle',
          actions: assign({ msg: (ctx, event) => event.data })
        },
        onError: {
          target: 'error',
          actions: assign({ msg: (ctx, event) => event.data })
        }
      },
      on: {
        CANCEL: 'idle',
        UPDATE_FAILED: 'error',
        UPDATED: 'idle'
      }
    },
    error: {
      on: {
        SUBMIT: [
          {
            target: "updating",
            cond: (ctx, event) => ((event.data.name !== '') && (event.data.email !== ''))
          },
          {
            target: "error",
            actions: assign({ msg: "Validation Failed" })
          }
        ]
      }
    }
  }
})

function App() {

  const [machine, send] = useMachine(profileStateMachine);
  const [form, updateForm] = useState({
    name: "",
    email: ""
  });

  console.log(machine.value);

  return (
    <div className="App">
      <div>
        <form onSubmit={
          e => {
            e.preventDefault();
            send({
              type: "SUBMIT",
              data: {
                ...form
              }
            })
          }
        }>
          <div>
            {(machine.matches('error')) ? (
              <h3 className="errorMsg"> {(machine.context.msg) ? machine.context.msg : "Something went wrong"} </h3>
            ) : null}

            {(machine.matches('idle') && (machine.context.msg)) ? (
              <h3 className="successMsg"> {machine.context.msg} </h3>
            ) : null}
          </div>
          <div className="formGroup">
            <label> Name </label>
            <input id="name" type="text" value={form.name} onChange={e => updateForm({ ...form, name: e.target.value })}/>
          </div>
          <div className="formGroup">
            <label> Email </label>
            <input id="email" type="email" value={form.email} onChange={e => updateForm({ ...form, email: e.target.value })} />
          </div>
          <button type="submit">
            UPDATE
          </button>
        </form>
      </div>
    </div>
  );
}

export default App;
